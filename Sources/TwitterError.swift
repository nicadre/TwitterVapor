//
//  TwitterError.swift
//  TwitterVapor
//
//  Created by Nicolas Chevalier Ignation on 28/03/2017.
//
//

public class TwitterError: Error {

	public let error: String

	private init(error: String) {
		self.error = error
	}

	public static var missingCredentials = TwitterError(error: "you need to provide credentials")
	public static var userStreamAlreadyStarted = TwitterError(error: "user stream is already started")

}
