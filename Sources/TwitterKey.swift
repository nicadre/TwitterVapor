//
//  TwitterKey.swift
//  TwitterVapor
//
//  Created by Nicolas Chevalier Ignation on 28/03/2017.
//
//

public enum TwitterKey: String {
	case consumerKey = "consumer_key"
	case consumerSecret = "consumer_secret"
	case accessToken = "access_token"
	case accessTokenSecret = "access_token_secret"
}
