//
//  Stream.swift
//  TwitterVapor
//
//  Created by Nicolas Chevalier Ignation on 28/03/2017.
//
//

internal protocol Stream {

	var streamURL: String { get }

	var consumerKey: String { get }
	var consumerSecret: String { get }
	var accessToken: String { get }
	var accessTokenSecret: String { get }

	func start() throws
	func stop() throws

}
