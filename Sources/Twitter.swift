//
//  Twitter.swift
//  TwitterVapor
//
//  Created by Nicolas Chevalier on 28/03/2017.
//
//

public class Twitter {

	let consumerKey: String
	let consumerSecret: String
	let accessToken: String
	let accessTokenSecret: String

	init(credentials: [TwitterKey: String]) throws {
		guard
			let consumerKey = credentials[.consumerKey],
			let consumerSecret = credentials[.consumerSecret],
			let accessToken = credentials[.accessToken],
			let accessTokenSecret = credentials[.accessTokenSecret]
		else {
			throw TwitterError.missingCredentials
		}

		if consumerKey.isEmpty || consumerSecret.isEmpty || accessToken.isEmpty || accessTokenSecret.isEmpty {
			throw TwitterError.missingCredentials
		}

		self.consumerKey = consumerKey
		self.consumerSecret = consumerSecret
		self.accessToken = accessToken
		self.accessTokenSecret = accessTokenSecret
	}

}
