//
//  UserStreamDelegate.swift
//  TwitterVapor
//
//  Created by Nicolas Chevalier Ignation on 29/03/2017.
//
//

public protocol UserStreamDelegate {

	func streamClosed(_ stream: UserStream, withCode code: UInt16?, reason: String?, clean: Bool)

}
