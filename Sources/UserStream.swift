//
//  UserStream.swift
//  TwitterVapor
//
//  Created by Nicolas Chevalier Ignation on 28/03/2017.
//
//

import WebSockets

public class UserStream: Stream {

	internal let streamURL: String = "https://userstream.twitter.com/1.1/user.json"

	internal let consumerKey: String
	internal let consumerSecret: String
	internal let accessToken: String
	internal let accessTokenSecret: String

	public var delegate: UserStreamDelegate? = nil

	private var socket: WebSocket?

	internal init(consumerKey: String, consumerSecret: String, accessToken: String, accessTokenSecret: String) {
		self.consumerKey = consumerKey
		self.consumerSecret = consumerSecret
		self.accessToken = accessToken
		self.accessTokenSecret = accessTokenSecret
	}

	public func start() throws {
		if self.socket == nil {
			try WebSocket.connect(to: streamURL, headers: [:]) { ws in
				self.socket = ws

				self.socket?.onText = { _, text throws in

				}

				self.socket?.onClose = { _, code, reason, clean in
					self.delegate?.streamClosed(self, withCode: code, reason: reason, clean: clean)
				}
			}
		} else {
			throw TwitterError.userStreamAlreadyStarted
		}
	}

	public func stop() throws {
		try self.socket?.close(statusCode: 200, reason: "the stream was closed")
	}

}
